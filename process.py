import requests 
from bs4 import BeautifulSoup
import csv

#Request web content
result = requests.get('https://www-fars.nhtsa.dot.gov/Main/index.aspx')

#Save content in variable.
src = result.content

#Soup
soup = BeautifulSoup(src, 'lxml')

#Look for table and save in CSV
table = soup.findAll('table')
table2 = table[13]


with open('traffic.csv','w', newline = '') as f:
    writer = csv.writer(f)
    for tr in table2('tr')[:16]:
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell4 = row[3]
        row = [cell1,cell4]
        writer.writerow(row)
