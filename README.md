


In 2015, there were 32,538 people killed and an estimated
2,443,000 people injured in police-reported motor vehicle traffic crashes. 

Motor vehicle travel is a major means of transportation in the United States, providing an
unparalleled degree of mobility. Yet for all its advantages, motor vehicle crashes were the leading cause of death in 2015.
The mission of the [National Highway Traffic Safety Administration](https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812376) is to reduce deaths, injuries, and economic
losses from motor vehicle crashes.

The figures are preliminary estimates from the National Safety Council, which says it currently estimates that "38,000 people were killed on U.S. roads, and 4.4 million were seriously injured, meaning 2015 likely was the **deadliest driving year** since 2008."


The number of police-reported motor vehicle crashes and fatalities by occupants is presented in the chart below.

The most obvious shift is the number of passenger car occupant fatalities.

![](https://i.imgur.com/ZVaDRaw.png)


Both the NSC and NHTSA urge several steps to make driving safer, from cutting incidents of drunk driving to using seatbelts and avoiding driving when fatigued or distracted.

The rise in fatalities came despite reports last year that found marked improvements in vehicle safety.

## Resources:
https://www-fars.nhtsa.dot.gov/Main/index.aspx

## License:
This Data Package is made available under the Public Domain Dedication and License v1.0 whose full text can be found at: PPDL